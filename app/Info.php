<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    protected $fillable = [
        'fName', 'lName', 'email', 'phone', 'adr', 'zip', 'country', 'comments'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    protected $casts = [
    ];
}
