import Vue from 'vue';
import VueRouter from "vue-router";
import formContact from "./components/formContact";
import ExampleComponent from "./components/ExampleComponent";
import updateContacts from "./components/updateContacts";
import adminDashboard from "./components/adminDashboard";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: formContact
        },
        {
            path: '/contacts',
            name: 'contacts',
            component: updateContacts
        },
        {
            path: '/admin',
            name: 'admin',
            component: adminDashboard
        },
        {
            path: '/about',
            name: 'about',
            component: ExampleComponent
        }
    ]
});

export default router;
