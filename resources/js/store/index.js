import Vue from 'vue'
import Vuex, {mapState} from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        contacts: []
    },
    mutations: {
        setData(state, data) {
            state.contacts = data
        },
        addContact(state, contact) {
            state.contacts.push(contact)
        },
        deleteContact(state, contactId) {
            state.contacts = state.contacts.filter(el => el.id !== contactId);
        },
        updateContact(state, contact) {
            state.contacts[state.contacts.findIndex(el => el.id == contact.id)] = contact;
        }
    },
    actions: {}
})

export default store;
