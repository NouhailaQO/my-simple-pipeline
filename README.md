# Donation ISSATSO
> ### Test project (using vuejs, laravel).
----------

# Getting started

## Installation

Please check the official laravel and vuejs installation guides for server requirements before you start. [Official Laravel Documentation](https://laravel.com/docs/5.4/installation#installation) [Official VueJS Documentation](https://fr.vuejs.org/v2/guide/installation.html)


Clone the repository

    git clone git@gitlab.com:NouhailaQO/my-simple-pipeline.git

Switch to the repo folder

    cd test

Install all the dependencies using composer

    composer install

Install all the dependencies using npm

    npm install


Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000
 
**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)

    php artisan migrate
    php artisan serve

## Database seeding

**Populate the database with seed data with relationships which includes users, articles, comments, tags, favorites and follows. This can help you to quickly start testing the api or couple a frontend and start using it with ready content.**

Run the database seeder and you're done

    php artisan db:seed

***Note*** : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command

    php artisan migrate:refresh
    
